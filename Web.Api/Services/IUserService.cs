﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Models;

namespace Web.Api.Services
{
    public interface IUserService
    {
        String Authenticate(string username, string password);
        IEnumerable<User> GetAll();
    }
}
