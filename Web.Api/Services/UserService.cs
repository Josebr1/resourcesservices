﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Web.Api.Helpers;
using Web.Api.Models;

namespace Web.Api.Services
{
    public class UserService : IUserService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        private List<User> _users = new List<User>
        {
            new User { Id = 2035, FirstName = "Test", LastName = "User", Username = "test", Password = "test" },
        };

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public String Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.Username == password && x.Password == password);

            if (user == null)
                return null;

            return CreateTokenJWT(user.Id.ToString(), "sadasdasd");
        }

        private string CreateTokenJWT(string id, string refresh_token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
          
            var jwtDescription = new SecurityTokenDescriptor
            {
                //Issuer = "dominio.com",
                //Audience = "dominio.com",
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(JwtRegisteredClaimNames.Sub, id)
                }),
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddMinutes(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenEncoded = tokenHandler.CreateToken(jwtDescription);

            var response = new
            {
                access_token = tokenHandler.WriteToken(tokenEncoded),
                //refresh_token = "",
                token_type = "Bearer",
                expires_in = DateTime.UtcNow.AddMinutes(2).Millisecond
            };

            return JsonConvert.SerializeObject(response);
        }

        public IEnumerable<User> GetAll()
        {
            // return users without passwords
            return _users.Select(x => {
                x.Password = null;
                return x;
            });
        }
    }
}
